﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zycie3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Podaj wymiary planszy.");
            //int x = Convert.ToInt32(Console.ReadLine());    // Row
            //int y = Convert.ToInt32(Console.ReadLine());    // Column

            int x = 10;
            int y = 10;
           
            Plansza P = new Plansza(x, y);
            P.Test();
            P.Show();
            Console.ReadLine();

            while (true)
            {
                P.Iter();
                Console.Clear();
                Console.WriteLine("Iteracja: {0:d}", P.iteracja);
                P.Show();
                Console.ReadLine();
            }

        }
    }

    class Plansza
    {
        private bool[,] _plansza;
        private int _x;
        private int _y;
        public int iteracja = 0;


        public Plansza(int x, int y)
        {
            _plansza = new bool[x, y];
            _plansza.Initialize();
            _x=x;
            _y=y;
        }

        public void Iter()
        {
            int n = 0;

            bool[,] plansza_new = new bool[_x, _y];
            plansza_new.Initialize();

           
            for (int i = 0; i < _x; i++)
            {
                for (int j = 0; j < _y; j++)
                {
                    n = Count(i, j);

                    if (_plansza[i,j] == true && n >= 2 && n <= 3)
                        plansza_new[i, j] = true;


                    if (_plansza[i, j] == false && n == 3)
                        plansza_new[i, j] = true;
                }
            }

            _plansza = plansza_new;
            iteracja++;
        }

        private int Count(int x, int y)
        {
            // Poprawic
            int count = 0;
 
            int deltax = 0;
            int deltay = 0;

            int deltax2 = 0;
            int deltay2 = 0;

            if (x == 0)
                deltax = 1;
            
            if(x == _x-1)
                deltax2 = -1;

            if (y == 0)
                deltay = 1;
        
            if (y == _y - 1)
                deltay2 = -1;

            for (int k=deltax; k<3+deltax2; k++)
            {
                for (int l=deltay; l<3+deltay2; l++)
                {
                    if (_plansza[x - 1 + k, y - 1 + l] == true)
                    count++;
                }
            }

            if(_plansza[x,y] == true)
                count--;

            return count;
        }

        public void Test()
        {
            _plansza[1, 1] = true;
            _plansza[2, 2] = true;
            _plansza[1, 2] = true;
            _plansza[2, 1] = true;

            _plansza[4, 5] = true;
            _plansza[4, 6] = true;
            _plansza[4, 7] = true;

            _plansza[7, 1] = true;
            _plansza[7, 2] = true;
            _plansza[7, 3] = true;
            _plansza[8, 2] = true;
            _plansza[8, 3] = true;
            _plansza[8, 4] = true;
        }

        public void Show()
        {
            for (int i=0; i<_x; i++)
            {
                for (int j=0; j<_y; j++)
                {
                    if (_plansza[i,j] == true)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write("-");
                    }
                }

                Console.WriteLine("");
            }
        }

    }

}
